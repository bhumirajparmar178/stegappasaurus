import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  loaderContainer: {
    alignItems: "center",
    bottom: 0,
    flex: 1,
    justifyContent: "center",
    left: 0,
    opacity: 0.75,
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 2
  }
});

export default styles;
