import { StyleSheet } from "react-native";

import { fonts } from "~/modules";

const styles = StyleSheet.create({
  text: {
    fontFamily: fonts.bodyLight
  }
});

export default styles;
