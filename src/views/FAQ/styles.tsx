import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },

  faqListContainer: {
    alignItems: "stretch",
    padding: 10
  }
});

export default styles;
