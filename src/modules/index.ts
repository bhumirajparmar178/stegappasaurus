import { colors } from "./colors";
import { fonts } from "./fonts";
import { DARK_THEME, PRIMARY_THEME } from "./themes";

export { colors, PRIMARY_THEME, DARK_THEME, fonts };
