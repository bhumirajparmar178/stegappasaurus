export const fonts = {
  body: "Roboto",
  bodyLight: "Roboto-Light",
  header: "Roboto-Thin"
};
