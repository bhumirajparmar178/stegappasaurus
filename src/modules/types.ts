export type ThemeColors = "#17212D" | "#FFF";
export type PrimaryColor = "#009CFF" | "#E88C0C";

export interface ITheme {
  background: ThemeColors;
  color: ThemeColors;
  isDark: boolean;
}
