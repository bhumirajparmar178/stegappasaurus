export const colors = {
  blue: "#007CFE",
  darkBlue: "#17212D",
  primary: "#009CFF",
  pureBlack: "#000",
  pureWhite: "#FFF",
  secondary: "#E88C0C"
};
