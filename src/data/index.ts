import about from "./about";
import questions from "./questions";
import slides from "./slides";

export { about, questions, slides };
